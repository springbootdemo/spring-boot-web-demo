package com.example;

import com.example.domain.Reservation;
import com.example.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class SpringBootWebDemoApplication {

	@Autowired
	private ReservationRepository reservationRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner line() {
		return args -> {
			Reservation r1 = new Reservation();
			r1.setDeparture("AMS");
			r1.setDestination("NYE");
			reservationRepository.save(r1);

			Reservation r2 = new Reservation();
			r2.setDeparture("AMS");
			r2.setDestination("SYD");
			reservationRepository.save(r2);};
	}

}
