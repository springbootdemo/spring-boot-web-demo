package com.example.controller;

import com.example.domain.Reservation;
import com.example.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/reservations")
public class ReservationWebController  {

    public ReservationRepository reservationRepository;

    @Autowired
    public ReservationWebController(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @RequestMapping(method=RequestMethod.GET)
    public String readersBooks(Model model) {

        List<Reservation> reservations = reservationRepository.findAll();
        model.addAttribute("reservationList", reservations);

        return "reservations";
    }

    @RequestMapping(method=RequestMethod.POST)
    public String addToReadingList(Reservation reservation) {
        reservationRepository.save(reservation);
        return "redirect:/reservations.html";
    }

}
