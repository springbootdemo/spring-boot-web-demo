# Introduction

Spring Boot example using:
* Spring Web
* Spring Data
* H2
* Actuator

# How to build the application
`mvn clean package`

# How to run the application
`mvn spring-boot:run`